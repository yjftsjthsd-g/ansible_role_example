Example ansible role
=========

Example ansible role; not terribly useful now, but used to be a template and reminder of the basics.


Example Playbook
----------------

```
    - hosts: servers
      roles:
         - { role: example }
```


License
-------

Public Domain


